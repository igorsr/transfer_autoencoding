from deap import benchmarks
import numpy as np
from math import sqrt



def metal_cutting(individual):

    # variables	
    v = individual[0]  # cutting speed
    f = individual[1]  # feed rate 
    a = individual[2]  # depth of cut

    # constant values			   
    PMAX = 10.0 # motor potency
    FcMAX = 5000.0 # maximum cutting force 
    N = 0.75 # motor transmission efficiency 
    rn = 0.8 # nose radius 
    RMAX = 50 # surface roughness

	
    # auxiliary values 
    #double T, MMR, P, Fc, R

    MMR = 1000 * v*f*a
    T   = (5.48*1000000000.0) / ( (v ** 3.46) * (f ** 0.696) * (a ** 0.460))
    R   = (125.0*f*f) / rn
    Fc  = (6.56 * 1000  * (f ** 0.917) * (a ** 1.10)) / (v ** 0.286)
    P   = (v*Fc) / (60000.0)

    # constraints 
    constr1 = N*PMAX - P
    constr2 = FcMAX  - Fc
    constr3 = RMAX   - R

    individual.fitness.constraint_violation = 0.0
    for constraint in [constr1, constr2, constr3]:
        if constraint < 0.0:
            individual.fitness.constraint_violation += abs(constraint)


    # objectives 
    obj1 = 0.15 + 219912 * ((1.0 + 0.20/T) / MMR) + 0.05
    obj2 = (219912.0 / (MMR*T))*100.0

    return obj1, obj2


def calcula_restricoes_metal_cutting(individual):
	
    # variables	
    v = individual[0]  # cutting speed
    f = individual[1]  # feed rate 
    a = individual[2]  # depth of cut

    # constant values			   
    PMAX = 10.0        # motor potency 
    FcMAX = 5000.0     # maximum cutting force 
    N = 0.75  		   # motor transmission efficiency 
    rn = 0.8           # nose radius 
    RMAX = 50          # surface roughness


    # auxiliary values 
    #double T, MMR, P, Fc, R

    MMR = 1000 * v*f*a
    T   = (5.48*1000000000.0) / ( (v ** 3.46) * (f ** 0.696) * (a ** 0.460))
    R   = (125.0*f*f) / rn
    Fc  = (6.56 * 1000  * (f ** 0.917) * (a ** 1.10)) / (v ** 0.286)
    P   = (v*Fc) / (60000.0)

    # constraints 
    constr1 = N*PMAX - P
    constr2 = FcMAX  - Fc
    constr3 = RMAX   - R

    individual.fitness.constraint_violation = 0.0
    for constraint in [constr1, constr2, constr3]:
        if constraint < 0.0:
            individual.fitness.constraint_violation += abs(constraint)


    return individual.fitness.constraint_violation


def calcula_restricoes_welded_beam(individual):
    #double b,t,l,h;
    h = individual[0]
    b = individual[1]
    l = individual[2]
    t = individual[3]

	# constraint
	#double Sigma, Tal, Tal_1, Tal_2, Pc;

    Tal_1 = 6000.0 / (sqrt(2)*h*l)
    Tal_2 = 6000.0*(14.0+0.5*l)*sqrt(0.25*(l*l+(h+t)*(h+t))) / ( 2.0 * (0.707*h*l*((l*l)/12.0 + 0.25*(h+t)*(h+t))))
    Tal = sqrt( (Tal_1*Tal_1) + (Tal_2*Tal_2) + (l*Tal_1*Tal_2) / sqrt(0.25*(l*l+(h+t)*(h+t))) )

    Sigma = 504000.0 / (t*t*b)
    Pc = 64746.022*(1.0-0.0282346*t)*t*b*b*b

    constr1 = 13600.0 - Tal
    constr2 = 30000.0 - Sigma
    constr3 = b-h
    constr4 = Pc - 6000.0

    individual.fitness.constraint_violation = 0.0
    for constraint in [constr1, constr2, constr3, constr4]:
        if constraint < 0.0:
            individual.fitness.constraint_violation += abs(constraint)

    return individual.fitness.constraint_violation
    

def welded_beam(individual):

    #double b,t,l,h;
    h = individual[0]
    b = individual[1]
    l = individual[2]
    t = individual[3]

	# constraint
	#double Sigma, Tal, Tal_1, Tal_2, Pc;

    Tal_1 = 6000.0 / (sqrt(2)*h*l)
    Tal_2 = 6000.0*(14.0+0.5*l)*sqrt(0.25*(l*l+(h+t)*(h+t))) / ( 2.0 * (0.707*h*l*((l*l)/12.0 + 0.25*(h+t)*(h+t))))
    Tal = sqrt( (Tal_1*Tal_1) + (Tal_2*Tal_2) + (l*Tal_1*Tal_2) / sqrt(0.25*(l*l+(h+t)*(h+t))) )

    Sigma = 504000.0 / (t*t*b)
    Pc = 64746.022*(1.0-0.0282346*t)*t*b*b*b

    constr1 = 13600.0 - Tal
    constr2 = 30000.0 - Sigma
    constr3 = b-h
    constr4 = Pc - 6000.0

    individual.fitness.constraint_violation = 0.0
    for constraint in [constr1, constr2, constr3, constr4]:
        if constraint < 0.0:
            individual.fitness.constraint_violation += abs(constraint)


    # objectives

    obj1 = 1.10471*(h*h)*l + 0.04811*t*b*(14.0+l)
    obj2 = 2.1952 / (t*t*t*b)

    return obj1, obj2




def two_member_truss(individual):
    """ TWO-MEMBER TRUSS DESIGN
    of real variables = 3
    of bin variables = 0
    of objectives = 2
    of constraints = 1
    """

    obj = np.zeros(2)
    stressAC = 0.0
    stressBC = 0.0
    maxStress = 0.0
    SMAX = 100000.0

    stressAC = (20.0*sqrt(16.0 + individual[2]*individual[2]))/(individual[2]*individual[0])
    stressBC = (80.0*sqrt(1.0  + individual[2]*individual[2]))/(individual[2]*individual[1])

    if stressAC > stressBC:
        maxStress = stressAC
    else:
        maxStress = stressBC

    obj[0] = (individual[0]*sqrt(16.0 + individual[2]*individual[2])) + (individual[1]*sqrt(1.0 + individual[2]*individual[2]))
    obj[1] = maxStress


    if np.isnan(obj[0]) or np.isnan(obj[1]):
        individual.fitness.constraint_violation = 10**10

    elif SMAX-maxStress >= 0.0:
        individual.fitness.constraint_violation = 0.0
    else:
        individual.fitness.constraint_violation = abs(SMAX-maxStress)

	#constr[0] = 1.0 - maxStress/SMAX
    #print(obj)

    return obj[0], obj[1]




def calcula_restricoes_two_member_truss(individual):
    """ TWO-MEMBER TRUSS DESIGN
    of real variables = 3
    of bin variables = 0
    of objectives = 2
    of constraints = 1
    """

    obj = np.zeros(2)
    stressAC = 0.0
    stressBC = 0.0
    maxStress = 0.0
    SMAX = 100000.0

    stressAC = (20.0*sqrt(16.0 + individual[2]*individual[2]))/(individual[2]*individual[0])
    stressBC = (80.0*sqrt(1.0  + individual[2]*individual[2]))/(individual[2]*individual[1])

    if stressAC > stressBC:
        maxStress = stressAC
    else:
        maxStress = stressBC

    #obj[0] = (individual[0]*sqrt(16.0 + individual[2]*individual[2])) + (individual[1]*sqrt(1.0 + individual[2]*individual[2]))
    #obj[1] = maxStress;	

    if SMAX-maxStress >= 0.0:
        individual.fitness.constraint_violation = 0.0
    else:
        individual.fitness.constraint_violation = abs(SMAX-maxStress)



    return individual.fitness.constraint_violation

def feasible_two_member_truss(individual):
    """Feasibility function for the individual. Returns True if feasible False
    otherwise."""

    stressAC, stressBC, maxStress = 0.0, 0.0, 0.0;
    SMAX = 100000.0

    stressAC = (20.0*sqrt(16.0 + individual[2]*individual[2]))/(individual[2]*individual[0]);
    stressBC = (80.0*sqrt(1.0  + individual[2]*individual[2]))/(individual[2]*individual[1]);

    if stressAC > stressBC:
        maxStress = stressAC
    else:
        maxStress = stressBC

    #constr = 1.0 - maxStress/SMAX

    #print(individual)
    #print("MaxStress= " + str(maxStress))

    if SMAX-maxStress >= 0.0:
        return True
    else:
        return False


def obtem_definicao_problema(problem):
    # Problem definition
    
    # Functions zdt4 has bounds x1 = [0, 1], xn = [-5, 5], with n = 2, ..., 10
    # BOUND_LOW, BOUND_UP = [0.0] + [-5.0]*9, [1.0] + [5.0]*9

    # Functions zdt1, zdt2, zdt3, zdt6 have bounds [0, 1]
    # Functions zdt1, zdt2, zdt3 have 30 dimensions, zdt4 and zdt6 have 10

    NDIM, NOBJ, BOUND_LOW, BOUND_UP = 0, 0, [], []

    FEASIBILITY_FUNCTION = None

    if problem == benchmarks.zdt1 or \
    problem == benchmarks.zdt2 or \
    problem == benchmarks.zdt3:

        NDIM = 30
        NOBJ = 2     

        for i in range(0, NDIM):
            BOUND_LOW.append(0.0)
            BOUND_UP.append(1.0)
    
    if problem == benchmarks.zdt4:

        NDIM = 10
        NOBJ = 2        

        BOUND_LOW.append(0.0)
        BOUND_UP.append(1.0)

        for i in range(0, NDIM):
            BOUND_LOW.append(-5.0)
            BOUND_UP.append(5.0)

    if problem == benchmarks.zdt6:

        NDIM = 10
        NOBJ = 2

        for i in range(0, NDIM):
            BOUND_LOW.append(0.0)
            BOUND_UP.append(1.0)

    if problem == benchmarks.dtlz1:

        NDIM = 7
        NOBJ = 3
        
        for i in range(0, NDIM):
            BOUND_LOW.append(0.0)
            BOUND_UP.append(1.0)

    if problem == benchmarks.dtlz2:

        NDIM = 12
        NOBJ = 3
        
        for i in range(0, NDIM):
            BOUND_LOW.append(0.0)
            BOUND_UP.append(1.0)

    if problem == benchmarks.dtlz3:

        NDIM = 12
        NOBJ = 3
        
        for i in range(0, NDIM):
            BOUND_LOW.append(0.0)
            BOUND_UP.append(1.0)

    if problem == benchmarks.dtlz4:
        NDIM = 12
        NOBJ = 3
        
        for i in range(0, NDIM):
            BOUND_LOW.append(0.0)
            BOUND_UP.append(1.0)


    if problem == benchmarks.dtlz5:
        NDIM = 12
        NOBJ = 3
        
        for i in range(0, NDIM):
            BOUND_LOW.append(0.0)
            BOUND_UP.append(1.0)

    if problem == benchmarks.dtlz6:
        NDIM = 12
        NOBJ = 3
        
        for i in range(0, NDIM):
            BOUND_LOW.append(0.0)
            BOUND_UP.append(1.0)

    if problem == benchmarks.dtlz7:
        NDIM = 22
        NOBJ = 3
        
        for i in range(0, NDIM):
            BOUND_LOW.append(0.0)
            BOUND_UP.append(1.0)


    if problem == two_member_truss:

        NDIM = 3
        NOBJ = 2

        BOUND_LOW.append(0.0)
        BOUND_UP.append(0.01)

        BOUND_LOW.append(0.0)
        BOUND_UP.append(0.01)

        BOUND_LOW.append(1.0)
        BOUND_UP.append(3.0)

        FEASIBILITY_FUNCTION = feasible_two_member_truss

    if problem == welded_beam:

        NDIM = 4
        NOBJ = 2

        BOUND_LOW.append(0.125) # h
        BOUND_UP.append(5.0) 

        BOUND_LOW.append(0.125) # b
        BOUND_UP.append(5.0)

        BOUND_LOW.append(0.1) # l
        BOUND_UP.append(10.0)

        BOUND_LOW.append(0.1) # t
        BOUND_UP.append(10.0)        

        #print(BOUND_LOW)
        #print(BOUND_UP)

        # FEASIBILITY_FUNCTION = feasible_welded_beam

    if problem == metal_cutting:

        NDIM = 3
        NOBJ = 2

        BOUND_LOW.append(250.0) # v
        BOUND_UP.append(400.0) 

        BOUND_LOW.append(0.15) # f
        BOUND_UP.append(0.55)

        BOUND_LOW.append(0.5) # a
        BOUND_UP.append(6.0)

    


    return NDIM, NOBJ, BOUND_LOW, BOUND_UP, problem