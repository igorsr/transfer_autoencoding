from keras.layers import Input, Dense
from keras.models import Model
import numpy as np

from sklearn.model_selection import train_test_split
from sklearn.model_selection import StratifiedKFold
from sklearn.model_selection import KFold

autoencoder,encoder, decoder = None, None, None
criou_autoencoder = False

def GetMaiorValorColuna(dataSet, coluna):
    """Retorna o maior valor de uma feature do dataset."""

    max = dataSet[0][coluna]
    
    for i in range(1, dataSet.shape[0]):
        if dataSet[i][coluna] > max:
            max = dataSet[i][coluna]
    return max


def RecriarAutoencoder():

    global criou_autoencoder
    criou_autoencoder = False    
    #autoencoder.compile(optimizer='adadelta', loss='mean_squared_error')


def CriaAutoEncoder(input_dim=30, encoding_dim=10):

    global criou_autoencoder
    global autoencoder,encoder,decoder

    encoding_dim = int(input_dim/2)

    if not criou_autoencoder:

        print("Recriando autoencoder (cria_autoencoder.py)")

        """
        # this is our input placeholder
        input_img = Input(shape=(input_dim,))

        # "encoded" is the encoded representation of the input
        encoded = Dense(encoding_dim, activation='relu')(input_img)

        # "decoded" is the lossy reconstruction of the input
        #decoded = Dense(input_dim, activation='sigmoid')(encoded)

        # this model maps an input to its reconstruction
        #autoencoder = Model(input_img, decoded)

        # this model maps an input to its encoded representation
        encoder = Model(input_img, encoded)

        encoder.compile(optimizer='adadelta', loss='mean_squared_error')
        """

        # this is the size of our encoded representations

        # this is our input placeholder
        input_img = Input(shape=(input_dim,))

        # "encoded" is the encoded representation of the input
        encoded = Dense(encoding_dim, activation='relu')(input_img)
        # "decoded" is the lossy reconstruction of the input
        decoded = Dense(input_dim, activation='sigmoid')(encoded)
        #decoded = Dense(input_dim, activation='softplus')(encoded)

        # this model maps an input to its reconstruction
        autoencoder = Model(input_img, decoded)

        # this model maps an input to its encoded representation
        encoder = Model(input_img, encoded)

        # create a placeholder for an encoded (32-dimensional) input
        encoded_input = Input(shape=(encoding_dim,))
        # retrieve the last layer of the autoencoder model
        decoder_layer = autoencoder.layers[-1]
        # create the decoder model
        decoder = Model(encoded_input, decoder_layer(encoded_input))

        #autoencoder.compile(optimizer='adadelta', loss='binary_crossentropy')

        autoencoder.compile(optimizer='adadelta', loss='mean_squared_error')

        #autoencoder = encoder    
        criou_autoencoder = True

    return autoencoder, encoder, decoder


def MapeiaSolucoesProblemasAnteriores(S1, DIM_S, pop_atual, g):

    geracoes = int(GetMaiorValorColuna(S1, 0))

    TAMANHO_POPULACAO = len(pop_atual)
    D_S1 = DIM_S
    D_S2 = len(pop_atual[0])

    """
        Se a dimensão dos datasets for diferente, é necessário acrescentar zeros à direita ao 
        conjunto de menor dimensão.
    """

    D = np.max([D_S1, D_S2])

    #print("Shape s1 = " + str(D_S1))
    #print("Shape s2 = " + str(D_S2))
    #print("Shape D = " + str(D))
    #print("tamanho pop: " + str(TAMANHO_POPULACAO))

    PP_S1_G = np.ndarray(shape=(TAMANHO_POPULACAO, D), dtype=float, order='F')
    PP_S2_G = np.ndarray(shape=(TAMANHO_POPULACAO, D), dtype=float, order='F')

    #conterá a última população obtida para S
    PP_S1_Optimized_Solutions = np.ndarray(shape=(TAMANHO_POPULACAO, D), dtype=float, order='F')

    i_dataset = (geracoes-1)*TAMANHO_POPULACAO
    for i in range(0, TAMANHO_POPULACAO):                
        #print("\t" + str(i_dataset+i))
        for j in range(0, D):
            if j < D_S1:
                PP_S1_Optimized_Solutions[i][j] = S1[i_dataset+i][j+2]
            else:
                PP_S1_Optimized_Solutions[i][j] = 0.0


    i_dataset = g*TAMANHO_POPULACAO
    for i in range(0, TAMANHO_POPULACAO):                    
        for j in range(0, D):

            if j < D_S1:
                PP_S1_G[i][j] = S1[i_dataset+i][j+2]
            else:
                PP_S1_G[i][j] = 0.0

            if j < D_S2:        
                PP_S2_G[i][j] = pop_atual[i][j]
            else:
                PP_S2_G[i][j] = 0.0

    
    X_train, X_test, Y_train, Y_test = train_test_split(PP_S1_G, PP_S2_G, test_size=0.1, random_state=42)

    autoencoder, encoder, decoder = CriaAutoEncoder(input_dim=D)

    #autoencoder.compile(optimizer='adadelta', loss='mean_squared_error')
    
    autoencoder.fit(X_train, Y_train,
                    epochs=50,
                    batch_size=TAMANHO_POPULACAO,
                    shuffle=True,
                    validation_data=(X_test, Y_test), verbose=False)

    test_loss = autoencoder.evaluate(X_test, Y_test, verbose=False)

    #print('Loss:', test_loss)

    predicao = autoencoder.predict(PP_S1_Optimized_Solutions)

    if D_S1 != D_S2:
        learned_solutions = np.ndarray(shape=(len(predicao), D_S2), dtype=float, order='F')

        for i in range(0, len(predicao)):
            for j in range(0, D_S2):
                learned_solutions[i][j] = predicao[i][j]
    else:
        learned_solutions = predicao
    

    return learned_solutions