from keras.layers import Input, Dense
from keras.models import Model
import numpy as np

from sklearn.model_selection import train_test_split
from sklearn.model_selection import StratifiedKFold
from sklearn.model_selection import KFold

def GetMaiorValorColuna(dataSet, coluna):
    """Retorna o maior valor de uma feature do dataset."""

    max = dataSet[0][coluna]
    
    for i in range(1, dataSet.shape[0]):
        if dataSet[i][coluna] > max:
            max = dataSet[i][coluna]
    return max


class OptimizationProblemData(object):
    
    def __init__(self, _name, _decision_space_dimension, _objective_space_dimension, _pop_size, _generations):
        self.name = _name
        self.decision_space_dimension  = _decision_space_dimension
        self.objective_space_dimension = _objective_space_dimension
        self.population_size = _pop_size
        self.generations = _generations
        self.optimized_solutions = np.ndarray(shape=(self.population_size, self.decision_space_dimension), dtype=float, order='F')
        self.gth_population = np.ndarray(shape=(self.population_size, self.decision_space_dimension), dtype=float, order='F')
        self.population = np.ndarray(shape=(self.population_size*self.generations, self.decision_space_dimension+2), dtype=float, order='F')


    def load_population_from_text_file(self, fileName):

        self.population = np.loadtxt(fileName)        
        
        i_dataset = (self.generations-1)*self.population_size

        for i in range(0, self.population_size):
            for j in range(0, self.decision_space_dimension):
                self.optimized_solutions[i][j] = self.population[i_dataset+i][j+2]
                #print(self.optimized_solutions[i][j], end="\t")
            #print("")
           
        #print(self.optimized_solutions[0])
    
    def set_gth_population(self, g, pop):

        i_dataset = (g-1)*self.population_size

        for i in range(0, self.population_size):
            for j in range(0, self.decision_space_dimension):
                self.population[i_dataset+i][j+2] = pop[i][j]

        return None


    def get_gth_population(self, g):

        i_dataset = (g-1)*self.population_size

        for i in range(0, self.population_size):
            for j in range(0, self.decision_space_dimension):
                self.gth_population[i][j] = self.population[i_dataset+i][j+2]

        return self.gth_population  