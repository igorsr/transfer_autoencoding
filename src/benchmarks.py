import time, array, random, copy, math
import numpy as np
import pandas as pd
import matplotlib.pyplot as plt
plt.rc('text', usetex=True)
plt.rc('font', family='serif')
plt.rcParams['text.latex.preamble'] ='\\usepackage{libertine}\n\\usepackage[utf8]{inputenc}'

import seaborn
seaborn.set(style='whitegrid')
seaborn.set_context('notebook')

from deap import algorithms, base, benchmarks, tools, creator

random.seed(a=42)

creator.create("FitnessMin", base.Fitness, weights=(-1.0, -1.0))
creator.create("Individual", array.array, typecode='d', fitness=creator.FitnessMin)

def dent(individual, lbda = 0.85):
    """ 
    Implements the test problem Dent
    Num. variables = 2; bounds in [-1.5, 1.5]; num. objetives = 2.
    @author Cesar Revelo
    """
    d  = lbda * math.exp(-(individual[0] - individual[1]) ** 2)  
    f1 = 0.5 * (math.sqrt(1 + (individual[0] + individual[1]) ** 2) + \
                math.sqrt(1 + (individual[0] - individual[1]) ** 2) + \
                individual[0] - individual[1]) + d
    f2 = 0.5 * (math.sqrt(1 + (individual[0] + individual[1]) ** 2) + \
                math.sqrt(1 + (individual[0] - individual[1]) ** 2) - \
                individual[0] + individual[1]) + d
    return f1, f2            


def uniform(low, up, size=None):
    try:
        return [random.uniform(a, b) for a, b in zip(low, up)]
    except TypeError:
        return [random.uniform(a, b) for a, b in zip([low] * size, [up] * size)]



# toolbox = base.Toolbox()
# NDIM = 30 
# BOUND_LOW, BOUND_UP = 0.0, 1.0
# toolbox.register("evaluate", lambda ind: benchmarks.dtlz3(ind, 2))
# toolbox.register("attr_float", uniform, BOUND_LOW, BOUND_UP, NDIM)
# toolbox.register("individual", tools.initIterate, creator.Individual, toolbox.attr_float)
# toolbox.register("population", tools.initRepeat, list, toolbox.individual)
# toolbox.register("mate", tools.cxSimulatedBinaryBounded, low=BOUND_LOW, up=BOUND_UP, eta=20.0)
# toolbox.register("mutate", tools.mutPolynomialBounded, low=BOUND_LOW, up=BOUND_UP, eta=20.0, indpb=1.0/NDIM)
# toolbox.register("select", tools.selNSGA2)

# toolbox.pop_size = 50
# toolbox.max_gen = 2000
# toolbox.mut_prob = 0.2

# stats = tools.Statistics()
# stats.register("pop", copy.deepcopy)


# res,logbook = run_ea(toolbox)                                     


# benchmark

problem_instances = {'ZDT1': benchmarks.zdt1, 
                     'ZDT2': benchmarks.zdt2,
                     #'ZDT3': benchmarks.zdt3, 
                     #'ZDT4': benchmarks.zdt4,
                     #'ZDT6': benchmarks.zdt6,
                     #'DTLZ1': lambda ind: benchmarks.dtlz1(ind,3),
                     #'DTLZ2': lambda ind: benchmarks.dtlz2(ind,3),
                     #'DTLZ3': lambda ind: benchmarks.dtlz3(ind,3),
                     #'DTLZ4': lambda ind: benchmarks.dtlz4(ind,3, 100),
                     #'DTLZ5': lambda ind: benchmarks.dtlz5(ind,3),
                     #'DTLZ6': lambda ind: benchmarks.dtlz6(ind,3),
                     #'DTLZ7': lambda ind: benchmarks.dtlz7(ind,3)}
}

toolbox = base.Toolbox()

BOUND_LOW, BOUND_UP = 0.0, 1.0
#toolbox.register("evaluate", lambda ind: benchmarks.dtlz3(ind, 2))
NDIM = 3
toolbox.register("attr_float", uniform, BOUND_LOW, BOUND_UP, NDIM)
toolbox.register("individual", tools.initIterate, creator.Individual, toolbox.attr_float)
toolbox.register("population", tools.initRepeat, list, toolbox.individual)
toolbox.register("mate", tools.cxSimulatedBinaryBounded, low=BOUND_LOW, up=BOUND_UP, eta=20.0)
toolbox.register("mutate", tools.mutPolynomialBounded, low=BOUND_LOW, up=BOUND_UP, eta=20.0, indpb=1.0/NDIM)
toolbox.register("select", tools.selNSGA2)

toolbox.pop_size = 50
toolbox.max_gen = 1000
toolbox.mut_prob = 0.2

stats = tools.Statistics(lambda ind: ind.fitness.values)
stats.register("obj_vals", np.copy)

def run_ea(toolbox, stats=None, verbose=False):
    pop = toolbox.population(n=toolbox.pop_size)
    pop = toolbox.select(pop, len(pop))
    return algorithms.eaMuPlusLambda(pop, toolbox, mu=toolbox.pop_size, 
                                     lambda_=toolbox.pop_size, 
                                     cxpb=1-toolbox.mut_prob,
                                     mutpb=toolbox.mut_prob, 
                                     stats=stats, 
                                     ngen=toolbox.max_gen, 
                                     verbose=verbose)


def run_problem(toolbox, problem):
    toolbox.register('evaluate', problem)
    return run_ea(toolbox, stats=stats)

results = {problem: run_problem(toolbox, problem_instances[problem]) for problem in problem_instances}                    

import pickle
pickle.dump(results, open('nsga_ii_results.pickle', 'wb'))    

loaded_results = pickle.load(open('nsga_ii_results.pickle', 'rb'))
results = loaded_results #  <-- uncomment if needed

res = pd.DataFrame(results)

print(res.head())