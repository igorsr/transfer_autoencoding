from keras.layers import Input, Dense
from keras.models import Model
from keras import backend as K
import keras

def custom_loss(encoded_d1, encoded_d2):
    def loss(y_true, y_pred):
        a = K.categorical_crossentropy(y_true, y_pred)
        a *= 0 # we don't need a, but without it, "An operation has `None` for gradient" error rises up
        b = 0
        for i in range(128):  # you have to set batch_size = 128
            b += K.mean(K.square(encoded_d1[i] - encoded_d2[i]))
                 
        return a + b
    return loss


entrada_dataset_1 = Input(shape=(50,))
entrada_dataset_2 = Input(shape=(50,))

shared_dense = Dense(32, activation='relu')
encoded_d1   = shared_dense(entrada_dataset_1)
encoded_d2   = shared_dense(entrada_dataset_2)

decoded   = Dense(50, activation='sigmoid')(encoded_d1)
predicted = Dense(3, activation='sigmoid')(decoded)

encoder     = Model([entrada_dataset_1, entrada_dataset_2], encoded_d1)
autoencoder = Model([entrada_dataset_1, entrada_dataset_2], [decoded, predicted])

autoencoder.compile(optimizer='adam', loss=custom_loss(entrada_dataset_1, entrada_dataset_2))

import numpy as np
d1 = np.random.rand(512, 50)
d2 = np.random.rand(512, 50)
m2 = np.random.rand(512, 50)
m3 = np.random.rand(512, 50)

y = np.random.randint(0, high=3, size=512)
y = keras.utils.to_categorical(y)

autoencoder.fit([x, m1, m2, m3], [d1, y], batch_size=128, epochs=30)