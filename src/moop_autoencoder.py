from keras.layers import Input, Dense
from keras.models import Model
import numpy as np


from sklearn.model_selection import train_test_split
# validacao cruzada
from sklearn.model_selection import StratifiedKFold
from sklearn.model_selection import KFold

def CriaAutoEncoder(input_dim=30, encoding_dim=10):

    # this is our input placeholder
    input_img = Input(shape=(input_dim,))

    # "encoded" is the encoded representation of the input
    encoded = Dense(encoding_dim, activation='relu')(input_img)

    # "decoded" is the lossy reconstruction of the input
    decoded = Dense(input_dim, activation='sigmoid')(encoded)

    # this model maps an input to its reconstruction
    #autoencoder = Model(input_img, decoded)

    # this model maps an input to its encoded representation
    encoder = Model(input_img, encoded)

    # # create a placeholder for an encoded (32-dimensional) input
    # encoded_input = Input(shape=(encoding_dim,))
    # # retrieve the last layer of the autoencoder model
    # decoder_layer = autoencoder.layers[-1]
    # # create the decoder model
    # decoder = Model(encoded_input, decoder_layer(encoded_input))

    return encoder


#import pandas as pd

#S1 = pd.read_csv("zdt1_30.dat", delimiter='\t')
#S2 = pd.read_csv("zdt1_10.dat", delimiter='\t')

#def mask(df, f):
#  return df[f(df)]

#print(S1.columns)
#geracoes = S1.iloc[:, 0].unique()

def GetMaiorValorColuna(dataSet, coluna):
    """Retorna o maior valor de uma feature do dataset."""

    max = dataSet[0][coluna]
    
    for i in range(1, dataSet.shape[0]):
        if dataSet[i][coluna] > max:
            max = dataSet[i][coluna]
    return max


S1 = np.loadtxt("zdt1_30.dat")
#print(S1) 
S2 = np.loadtxt("zdt2_30.dat")

geracoes = int(GetMaiorValorColuna(S1, 0))

TAMANHO_POPULACAO = 100
D_S1 = S1.shape[1]-1
D_S2 = S2.shape[1]-1

#print("Shape s1 = " + str(D_S1))
#print("Shape s2 = " + str(D_S2))

PP_S1_G = np.ndarray(shape=(TAMANHO_POPULACAO, D_S1), dtype=float, order='F')
PP_S2_G = np.ndarray(shape=(TAMANHO_POPULACAO, D_S2), dtype=float, order='F')

#conterá a última população obtida para S1
PP_S1_Optimized_Solutions = np.ndarray(shape=(TAMANHO_POPULACAO, D_S1), dtype=float, order='F')

i_dataset = (geracoes-1)*TAMANHO_POPULACAO
for i in range(0, TAMANHO_POPULACAO):                
    #print("\t" + str(i_dataset+i))
    for j in range(0, D_S1):
        PP_S1_Optimized_Solutions[i][j] = S1[i_dataset+i][j+1]    

for g in range(0, geracoes+1):

    """
    Para cada geração:
        - carrega os dois datasets: PP_S1_G conterá a g-ésima populacao de S1, \ 
        enquanto PP_S2_G conterá a g-ésima populacao de S2

        - cria autoencoder
        - treina utilizando PP_S1_G e PP_S2_G
        - obtem learned_solutions:
            utiliza o autoencoder treinado para predizer solucoes de S2, \ 
            a partir das soluções pareto-otimas encontradas para S1
    """

    if g % 10 == 0:

        print("Geracao " + str(g))
        i_dataset = g*TAMANHO_POPULACAO
        for i in range(0, TAMANHO_POPULACAO):
                        
            #print("\t" + str(i_dataset+i))
            for j in range(0, D_S1):
                PP_S1_G[i][j] = S1[i_dataset+i][j+1]
            for j in range(0, D_S2):
                PP_S2_G[i][j] = S2[i_dataset+i][j+1]

        X_train, X_test, Y_train, Y_test = train_test_split(PP_S1_G, PP_S2_G, test_size=0.1, random_state=42)

        autoencoder = CriaAutoEncoder(input_dim=D_S1, encoding_dim=D_S2)

        autoencoder.compile(optimizer='adadelta', loss='mean_squared_error')
        autoencoder.fit(X_train, Y_train,
                        epochs=50,
                        batch_size=TAMANHO_POPULACAO,
                        shuffle=True,
                        validation_data=(X_test, Y_test), verbose=False)

        test_loss = autoencoder.evaluate(X_test, Y_test, verbose=False)

        print('Loss:', test_loss)
        learned_solutions = autoencoder.predict(PP_S1_Optimized_Solutions)


#S1 = S1.astype('float32') / 256
#S2 = S2.astype('float32') / 256

#K = 1
#kf = KFold(n_splits=K)

#media_desempenho = 0.0


X_train, X_test, Y_train, Y_test = train_test_split(S1, S2, test_size=0.1, random_state=42)

autoencoder = CriaAutoEncoder(input_dim=30, encoding_dim=30)

autoencoder.compile(optimizer='adadelta', loss='mean_squared_error')
autoencoder.fit(X_train, Y_train,
                epochs=50,
                batch_size=256,
                shuffle=True,
                validation_data=(X_test, Y_test), verbose=False)

test_loss = autoencoder.evaluate(X_test, Y_test, verbose=False)

print('Loss:', test_loss)

learned_solutions = autoencoder.predict(X_test)

#print(learned_solutions)