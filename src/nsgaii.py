import array
import random
import json
import numpy

from math import sqrt
from deap import algorithms
from deap import base
from deap import benchmarks
from deap.benchmarks.tools import diversity, convergence, hypervolume
from deap import creator
from deap import tools
from definicao_problemas import *
from Learner import *

def uniform(low, up, size=None):
    try:
        return [random.uniform(a, b) for a, b in zip(low, up)]
    except TypeError:
        return [random.uniform(a, b) for a, b in zip([low] * size, [up] * size)]


def calculate_reference(results, epsilon=0.1):
    alldata = np.concatenate(np.concatenate(results.values))
    obj_vals = [toolbox.evaluate(ind) for ind in alldata]

    return np.max(obj_vals, axis=0) + epsilon


def main(toolbox, pop_output_file=None, seed=None, NGEN = 750, \
MU=100,CXPB = 0.9, MAX_NFE = 75000, ENABLE_TRANSFER = False, TRANSFER_CROSSOVER=False, \
learner = None, BOUND_LOW=[], BOUND_UP=[]):

    random.seed(seed)
    
    NFE = 0

    stats = tools.Statistics(lambda ind: ind.fitness.values)
    stats.register("min", np.min, axis=0)
    stats.register("max", np.max, axis=0)
    
    logbook = tools.Logbook()
    logbook.header = "gen", "evals", "std", "min", "avg", "max"
    
    #history = tools.History()

    pop = toolbox.population(n=MU)
    #history.update(pop)

    # Evaluate the individuals with an invalid fitness
    invalid_ind = [ind for ind in pop if not ind.fitness.valid]
    fitnesses = toolbox.map(toolbox.evaluate, invalid_ind)
    for ind, fit in zip(invalid_ind, fitnesses):
        ind.fitness.values = fit
        #print(fit)
        #print(ind.fitness.constraint_violation)
    
    # This is just to assign the crowding distance to the individuals
    # no actual selection is done
    pop = toolbox.select (pop, len(pop))    

    
    # Begin the generational process
    for gen in range(1, NGEN+1):

        print("Gen - " + str(gen))

        if ENABLE_TRANSFER and TRANSFER_CROSSOVER and gen % 10 == 0:
            # insere solucoes aprendidas por machine learning            
            
            for index,PP in enumerate(learner.past_problems):

                if learner.transfer_from_problem[index] == True:

                    M = learner.train(gen, pop, index)
                    learned_solutions = learner.map_optimized_solutions(pop, gen, index, M)
                    
                    if len(learned_solutions)>=1:
                        
                        pop_ml = toolbox.population(n=len(learned_solutions))
                
                        for i, p in enumerate(learned_solutions):
                            for j in range(0, len(learned_solutions[i])):
                                pop_ml[i][j] = learned_solutions[i][j]

                        fitnesses = toolbox.map(toolbox.evaluate, pop_ml)
                        for ind, fit in zip(pop_ml, fitnesses):
                            ind.fitness.values = fit

                        NFE += len(pop_ml)

                        #calcula frentes nao dominadas e crowding distance
                        pop = toolbox.select(pop + pop_ml, len(pop)+len(pop_ml))

                        #verifica se alguma solucao candidata transferida foi selecionada para a proxima populacao
                        # postive_transfer = False
                        # for solucao_transferida in pop_ml:
                        #     if solucao_transferida in pop:
                        #         postive_transfer = True
                        #         break                      
                        

        
        # Vary the population
        offspring = tools.selTournamentDCD(pop, len(pop))
        offspring = [toolbox.clone(ind) for ind in offspring]
        
        for ind1, ind2 in zip(offspring[::2], offspring[1::2]):
            if random.random() <= CXPB:
                toolbox.mate(ind1, ind2)
            
            toolbox.mutate(ind1)
            toolbox.mutate(ind2)
            del ind1.fitness.values, ind2.fitness.values
        
        # Evaluate the individuals with an invalid fitness
        invalid_ind = [ind for ind in offspring if not ind.fitness.valid]
        fitnesses = toolbox.map(toolbox.evaluate, invalid_ind)
        for ind, fit in zip(invalid_ind, fitnesses):
            ind.fitness.values = fit
            #print(fit)         

        NFE += len(invalid_ind)

        #for p in pop:
        #    p.fitness.constraint_violation = calcula_restricoes_welded_beam(p)        

        if ENABLE_TRANSFER and TRANSFER_CROSSOVER == False and gen % 10 == 0:
            # insere solucoes aprendidas por machine learning
            #learned_solutions = MapeiaSolucoesProblemasAnteriores(S1, DIM_S1, pop, gen)

            for index,PP in enumerate(learner.past_problems):
                learner.train(gen, pop, index)

            index = int(np.argmin(learner.loss_values))
            #for index,PP in enumerate(learner.past_problems):       

            if learner.transfer_from_problem[index] == True:

                M = learner.train(gen, pop, index)
                learned_solutions = learner.map_optimized_solutions(pop, gen, index, M)

                if len(learned_solutions) > 0:

                    for i,row in enumerate(learned_solutions):
                        mudancas=0
                        for j,val in enumerate(row):
                            # if val >=-1 and val < 0:
                            #     predicted[i][j] *= -1
                            #     mudancas+=1
                            # elif val < -1 or val > 1:
                            #     predicted[i][j] = random.uniform(0, 1)
                            #     mudancas+=1
                            if val < BOUND_LOW[j] or val > BOUND_UP[j]:
                                learned_solutions[i][j] = random.uniform(BOUND_LOW[j], BOUND_UP[j])
                                mudancas += 1


                    pop_ml = toolbox.population(n=len(learned_solutions))
            
                    for i, p in enumerate(learned_solutions):
                        for j in range(0, len(learned_solutions[i])):
                            pop_ml[i][j] = learned_solutions[i][j]


                    fitnesses = toolbox.map(toolbox.evaluate, pop_ml)
                    for ind, fit in zip(pop_ml, fitnesses):
                        ind.fitness.values = fit                        

                    NFE += len(pop_ml)
                    pop = toolbox.select(pop + offspring + pop_ml, MU)                    

                else:
                    pop = toolbox.select(pop + offspring, MU)
            else:
                    pop = toolbox.select(pop + offspring, MU)
        else:
            # Select the next generation population           
            pop = toolbox.select(pop + offspring, MU)


        for p in pop:
            p.fitness.constraint_violation = calcula_restricoes_two_member_truss(p)
            #p.fitness.constraint_violation = calcula_restricoes_welded_beam(p)            
            #p.fitness.constraint_violation = calcula_restricoes_metal_cutting(p)
        

        #fitnesses = toolbox.map(toolbox.evaluate, pop)
        #for ind, fit in zip(pop, fitnesses):
        #    ind.fitness.values = fit

     
        if pop_output_file != None:
            for p in pop:        
                pop_output_file.write(str(gen))
                pop_output_file.write("\t")
                pop_output_file.write(str(NFE)+"\t")

                if p.fitness.constraint_violation == 0.0:

                    for d in range(0, NDIM):
                        pop_output_file.write(str(p[d]))
                        pop_output_file.write("\t")
                    for f in range(0, NOBJ):
                        pop_output_file.write(str(p.fitness.values[f]))
                        pop_output_file.write("\t")
                else:                    
                    for d in range(0, NDIM):
                        pop_output_file.write(str(p[d]))
                        pop_output_file.write("\t")
                    for f in range(0, NOBJ):
                        pop_output_file.write(str(10**10))
                        pop_output_file.write("\t")
                   

                pop_output_file.write(str(p.fitness.constraint_violation))
                #pop_output_file.write("\t")
                
                pop_output_file.write("\n")

        if(NFE >= MAX_NFE):
            break
            
    # reference = calculate_reference(pop)
    # print("final hypervolume is %f" % hypervolume(pop, [10.0, 10.0]))

    return pop, logbook



def configura_toolbox(toolbox, NDIM, NOBJ, BOUND_LOW, BOUND_UP, funcao_objetivo):

    toolbox.register("attr_float", uniform, BOUND_LOW, BOUND_UP, NDIM)
    toolbox.register("individual", tools.initIterate, creator.Individual, toolbox.attr_float)
    toolbox.register("population", tools.initRepeat, list, toolbox.individual)
    
    if funcao_objetivo == benchmarks.dtlz7 or funcao_objetivo == benchmarks.dtlz2 \
    or funcao_objetivo == benchmarks.dtlz5 or funcao_objetivo == benchmarks.dtlz6:

        toolbox.register("evaluate", lambda ind: funcao_objetivo(ind, NOBJ))

    elif funcao_objetivo == benchmarks.dtlz4:
        toolbox.register("evaluate", lambda ind: funcao_objetivo(ind, NOBJ, alpha=100))
    else:
        toolbox.register("evaluate", lambda ind: funcao_objetivo(ind))


    # if funcao_objetivo == two_member_truss:
    #     toolbox.decorate("evaluate", tools.DeltaPenalty(feasible_two_member_truss, 10**8))
    # elif funcao_objetivo == welded_beam:
    #     toolbox.decorate("evaluate", tools.DeltaPenalty(feasible_welded_beam, 10**8, distance_welded_beam))


    toolbox.register("mate", tools.cxSimulatedBinaryBounded, \
    low=BOUND_LOW, up=BOUND_UP, eta=20.0)
    toolbox.register("mutate", tools.mutPolynomialBounded, \
    low=BOUND_LOW, up=BOUND_UP, eta=20.0, indpb=1.0/NDIM)
    toolbox.register("select", tools.selNSGA2)


import cProfile

if __name__ == "__main__":    

    np.random.seed(456798)

    seeds = np.random.randint(0, 999999, 10000)
    contador_execucao = 0

    # NGEN = int(75000/52)
    NGEN = 1443
    MU = 52
    CXPB = 0.9
    #MAX_NFE = 75000
    MAX_NFE = 15000
    ENABLE_TRANSFER    = True
    TRANSFER_CROSSOVER = False

    creator.create("FitnessMin", base.Fitness, weights=(-1.0, -1.0, -1.0))    
    creator.create("Individual", np.ndarray, fitness=creator.FitnessMin)

    toolbox = base.Toolbox()

    problem = two_member_truss

    NDIM, NOBJ, BOUND_LOW, BOUND_UP, funcao_objetivo = obtem_definicao_problema(problem)

    configura_toolbox(toolbox, NDIM, NOBJ, BOUND_LOW, BOUND_UP, funcao_objetivo)

    current_problem = OptimizationProblemData("two_member_truss", NDIM, NOBJ, MU, NGEN)
    learner = Learner(current_problem) 

    for pnumber in ['1', '2','3']:
        print(pnumber)
        pp = OptimizationProblemData("zdt" + pnumber, 30, 2, 52, 1443)
        pp.load_population_from_text_file("../output/zdt" + pnumber + "/nsga_0.dat")
        learner.add_past_problem(pp)

    for pnumber in ['4', '6']:
        print(pnumber)
        pp = OptimizationProblemData("zdt" + pnumber, 10, 2, 52, 1443)
        pp.load_population_from_text_file("../output/zdt" + pnumber + "/nsga_0.dat")
        learner.add_past_problem(pp)

    for pnumber in ['1']:
        print(pnumber)
        pp = OptimizationProblemData("dtlz" + pnumber, 7, 3, 52, 1442)
        pp.load_population_from_text_file("../output/dtlz" + pnumber + "/nsga_0.dat")
        learner.add_past_problem(pp)

    for pnumber in ['2', '3', '4', '5', '6']:
        print(pnumber)
        pp = OptimizationProblemData("dtlz" + pnumber, 12, 3, 52, 1442)
        pp.load_population_from_text_file("../output/dtlz" + pnumber + "/nsga_0.dat")
        learner.add_past_problem(pp)

    for pnumber in ['7']:
        print(pnumber)
        pp = OptimizationProblemData("dtlz" + pnumber, 22, 3, 52, 1442)
        pp.load_population_from_text_file("../output/dtlz" + pnumber + "/nsga_0.dat")
        learner.add_past_problem(pp)


    for i in range(0, 10):

        if ENABLE_TRANSFER:
            RecriarAutoencoder()
            learner.reset()

        np.random.seed(seeds[i])
        contador_execucao += 1

        arquivo_saida_pop = open("../output/two_member_truss_transfer_todos_min_sig/nsga_" + str(i) + ".dat", 'w')

        pop, stats = main(toolbox, arquivo_saida_pop, NGEN = NGEN, MU = MU, CXPB = CXPB, \
        MAX_NFE = MAX_NFE, ENABLE_TRANSFER = ENABLE_TRANSFER, TRANSFER_CROSSOVER = TRANSFER_CROSSOVER, \
        learner = learner, BOUND_LOW=BOUND_LOW, BOUND_UP = BOUND_UP)

        arquivo_saida_pop.close()


    # pr.disable()
    # pr.dump_stats("profiler.txt")
    # pr.print_stats()