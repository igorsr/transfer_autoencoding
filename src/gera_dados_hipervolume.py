import numpy as np
from deap import tools
import deap.benchmarks.tools as bt
import pandas as pd


def GetMaiorValorColuna(dataSet, coluna):
    """Retorna o maior valor de uma feature do dataset."""

    max = dataSet[0][coluna]
    
    for i in range(1, dataSet.shape[0]):
        if dataSet[i][coluna] > max:
            max = dataSet[i][coluna]
    return max


def is_pareto_efficient(costs):
    """
    :param costs: An (n_points, n_costs) array
    :return: A (n_points, ) boolean array, indicating whether each point is Pareto efficient
    """
    is_efficient = np.ones(costs.shape[0], dtype = bool)
    for i, c in enumerate(costs):
        if is_efficient[i]:
            is_efficient[is_efficient] = np.any(costs[is_efficient]<=c, axis=1)  # Remove dominated points
    return is_efficient


def calcula_hipervolume(S1, D_S1, TAMANHO_POPULACAO, arquivo_saida, reference_point = [1,1]):

    df = pd.DataFrame(S1)
    df = df[df.columns[1]]
    nfe = df.drop_duplicates().values

    #epsilon = 0.1
    #reference_point = [GetMaiorValorColuna(S1, 1 + D_S1)+ epsilon, GetMaiorValorColuna(S1, 1+D_S1 + 1)+ epsilon] 
    
    print(reference_point)

    PP_S1_G = np.ndarray(shape=(TAMANHO_POPULACAO, OBJ_S1), dtype=float, order='F')

    #plt.figure(figsize=(5,5))

    for g,n in enumerate(nfe):    

        #if g > 0:
        #    break

        #print("Gen - " + str(g))
        i_dataset = g*TAMANHO_POPULACAO

        for i in range(0, TAMANHO_POPULACAO):        
            for j in range(0, OBJ_S1):
                PP_S1_G[i][j] = S1[i_dataset+i][2 + D_S1 + j]

        #non_dom = tools.sortNondominated(PP_S1_G, k=len(PP_S1_G), first_front_only=True)[0]

        is_pareto_front = is_pareto_efficient(PP_S1_G)
        
        class Fitness:
            def __init__(self, v):
                self.wvalues = v

        class Individuo:
            def __init__(self, v):
                self.fitness = Fitness(v)

        non_dominated_front = [] 

        for i,ind in enumerate(PP_S1_G):
            if is_pareto_front[i]:
                non_dominated_front.append(Individuo(-ind))

        #print(non_dominated_front[0].fitness.wvalues)
        #print("num de nao dominados: " + str(len(non_dominated_front)))
                
        
        # if g < 10:
        #     import matplotlib.pyplot as plt

        #     plt.clf()

        #     plt.figure(figsize=(5,5))
        #     #for i,ind in enumerate(PP_S1_G):
        #     #    plt.plot(ind[0], ind[1], 'k.', ms=3, alpha=0.5)

        #     for i,ind in enumerate(non_dominated_front):
        #         #print(ind.fitness.wvalues[0], " " ,ind.fitness.wvalues[1])
        #         plt.plot(ind.fitness.wvalues[0], ind.fitness.wvalues[1], 'bo', alpha=0.74, ms=5)
                
        #         #if is_pareto_front[i]:
        #         #    plt.plot(ind[0], ind[1], 'bo', alpha=0.74, ms=5)

        #     plt.title('Pareto-optimal front - generation ' + str(g))
        #     plt.savefig('../output/graficos/'+ str(g))
        #     #plt.show()
     

        hv = bt.hypervolume(non_dominated_front, reference_point)    
        #print(hv)
        arquivo_saida.write(str(n)+"\t"+str(hv)+"\n")
        
        #plt.plot(n, hv, 'k.', ms=3, alpha=0.5)

    arquivo_saida.close()


from os import listdir
from os.path import isfile, join    

mypath = "../output/two_member_truss_transfer_todos_min_sig/"
arquivos_saida = [f for f in listdir(mypath) if isfile(join(mypath, f))]

#obtem ponto de referencia a partir das solucoes candidatas encontradas

#reference_point = [1.0, 1.0]
#reference_point = [333.9095, 17561.59]
reference_point = [1.0, 100000.0,]
#reference_point = [10.0, 30.0]
epsilon = 0.001

D_S1 = 3
OBJ_S1 = 2
TAMANHO_POPULACAO = 52


# for arquivo in arquivos_saida:
#     S1 = np.loadtxt(mypath + arquivo)
#     r_point = [0,0]

#     r_point[0] = GetMaiorValorColuna(S1, 2 + D_S1)+ epsilon 
#     r_point[1] = GetMaiorValorColuna(S1, 2 + D_S1 + 1)+ epsilon

#     if r_point[0] > reference_point[0]:
#         reference_point[0] = r_point[0]
#     if r_point[1] > reference_point[1]:
#         reference_point[1] = r_point[1]


for arquivo in arquivos_saida:

    S1 = np.loadtxt(mypath + arquivo)    

    arquivo_saida = open(mypath + "/hv/" + arquivo, 'w')
    calcula_hipervolume(S1, D_S1, TAMANHO_POPULACAO, arquivo_saida, reference_point)
    