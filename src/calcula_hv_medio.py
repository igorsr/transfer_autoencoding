from os import listdir
from os.path import isfile, join
import pandas as pd
import numpy as np

mypath = "../output/two_member_truss_transfer_todos_min_sig/hv/"
arquivos = [f for f in listdir(mypath) if isfile(join(mypath, f))]

df_hipervolume_medio = None
primeiro = True

for arquivo in arquivos:
    S = np.loadtxt(mypath+arquivo)
    df = pd.DataFrame(S)

    if not primeiro:
        df_hipervolume_medio = pd.concat([df_hipervolume_medio, df])
    else:
        df_hipervolume_medio = df
        primeiro = False

hvs = df_hipervolume_medio.groupby(df_hipervolume_medio.columns[0]).mean()

#hvs.to_csv
arquivo_saida = open(mypath + "media/hv.dat", "w")

# plt.figure(figsize=(5,5))
# plt.title("Hipervolume medio ao longo da otimização - NSGAII, problema ZDT1")
# plt.xlabel("NFE")
# plt.ylabel("Hipervolume")

for index, row in df.iterrows():
    nfe = row[0]
    hv  = row[1]

    arquivo_saida.write(str(nfe) + "\t" + str(hv) +"\n")

arquivo_saida.close()