import numpy as np
from numpy.linalg import pinv
from OptimizationProblemData import *
from cria_autoencoder import *
import math
import keras
import sys
import random

global autoencoder,encoder,decoder

def uniform(low, up, size=None):
    try:
        return [random.uniform(a, b) for a, b in zip(low, up)]
    except TypeError:
        return [random.uniform(a, b) for a, b in zip([low] * size, [up] * size)]


from keras import backend as K
_EPSILON = K.epsilon()

def custom_objective(y_true, y_pred):    

    X_HAT = K.clip(y_pred, _EPSILON, 1.0-_EPSILON)
    Y = K.clip(y_true, _EPSILON, 1.0-_EPSILON)

    #diff2 = (y_true - y_pred)**2
    #return K.mean(K.sum(diff2, axis = None))

    # for i in range(0, 52):
    #     #print(y_pred[i])
    #     aux = K.sqrt( K.sum( K.square( Y[i] - X_HAT[0])) )
    #     print(aux)

    #     for j in range(1, 52):
    #         aux = K.sqrt( K.sum( K.square( Y[i] - X_HAT[j])) )

    # ads


    minimum_squared_differences = 0.0
    
    #sys.exit()   

    P = 52 # tamanho do batch (definir como tamanho da populacao)

    a = 0
    b = 0
    
    for i in range(0, 52):
        min = K.sqrt( K.sum( K.square( Y[i] - X_HAT[0])) ) # norma L2

        for j in range(1, 52):
            aux = K.sqrt( K.sum( K.square( Y[i] - X_HAT[j])) ) # norma L2
            #if aux[0] < min[0]:
            #    min[0] = aux[0]
        
        minimum_squared_differences += min
        b = minimum_squared_differences

        #b += K.sqrt( K.sum( K.square( Y[i] - X_HAT[i])) ) # norma l2

    return (minimum_squared_differences**2)

    #return a+b

    #b += K.mean(K.square(encoded_d1[i] - encoded_d2[i]))
  


class Learner(object):
    
    def __init__(self, _current_problem):
        self.current_problem = _current_problem
        self.loss_values   = []
        self.past_problems = []
        self.autoencoders  = []
        self._first_training = True
        self.transfer_from_problem = []
        

    def add_past_problem(self, _pp):
        self.past_problems.append(_pp)
        self.loss_values.append(-1)
        self.transfer_from_problem.append(True)

      
    def cria_auto_encoder(self, input_dim=30):

        # this is the size of our encoded representations
        encoding_dim = math.ceil(input_dim*0.1)

        #print("input_dim = " + str(input_dim)) 
        #print("encoding_dim = " + str(encoding_dim)) 

        # this is our input placeholder
        input_img = Input(shape=(input_dim,))

        limit = math.sqrt(6.0/(input_dim + encoding_dim))

        # "encoded" is the encoded representation of the input
        encoded = Dense(encoding_dim, activation='sigmoid', \
        kernel_initializer=keras.initializers.RandomUniform(minval=-limit, maxval=limit, seed=None), \
        bias_initializer='zeros')(input_img)

        # "decoded" is the lossy reconstruction of the input
        decoded = Dense(input_dim, activation='sigmoid',\
        kernel_initializer=keras.initializers.RandomUniform(minval=-limit, maxval=limit, seed=None), \
        bias_initializer='zeros')(encoded)
        #decoded = Dense(input_dim, activation='softplus')(encoded)

        # this model maps an input to its reconstruction
        autoencoder = Model(input_img, decoded)

        # this model maps an input to its encoded representation
        encoder = Model(input_img, encoded)

        # create a placeholder for an encoded (32-dimensional) input
        encoded_input = Input(shape=(encoding_dim,))
        # retrieve the last layer of the autoencoder model
        decoder_layer = autoencoder.layers[-1]
        # create the decoder model
        decoder = Model(encoded_input, decoder_layer(encoded_input))

        #autoencoder.compile(optimizer='adadelta', loss='binary_crossentropy')
        #autoencoder.compile(optimizer='adadelta', loss='mean_squared_error')
        #autoencoder.compile(optimizer='adadelta', loss='mean_squared_error')
        #autoencoder.compile(optimizer='sgd', loss='cosine_proximity')
        #autoencoder.compile(optimizer='adadelta', loss='cosine_proximity')

        autoencoder.compile(optimizer='adadelta', loss='mean_squared_error')        

        #autoencoder = encoder    
        #criou_autoencoder = True

        return autoencoder


    def train(self, G, pop, index=0):

        if self._first_training:

            #create autoencoders for each past problem

            self.loss_values = []
            self.autoencoders = []

            for PP in self.past_problems:

                self.loss_values.append(-1)

                D = np.max([self.current_problem.decision_space_dimension, PP.decision_space_dimension])

                autoencoder = self.cria_auto_encoder(D)
                #autoencoder, encoder, decoder = CriaAutoEncoder(D)

                self.autoencoders.append(autoencoder)

            self._first_training = False


        self.current_problem.set_gth_population(G, pop)

        PP = self.past_problems[index]

        D_S1 = PP.decision_space_dimension
        D_S2 = self.current_problem.decision_space_dimension
        POP_SIZE = self.current_problem.population_size

        """
            Se a dimensão dos datasets for diferente, é necessário acrescentar zeros à direita ao 
            conjunto de menor dimensão.
        """

        D = np.max([D_S1, D_S2])

        _PP_S1_G = PP.get_gth_population(G)
        _PP_S2_G = self.current_problem.get_gth_population(G)

        PP_S1_G = np.ndarray(shape=(POP_SIZE, D), dtype=float, order='F')
        PP_S2_G = np.ndarray(shape=(POP_SIZE, D), dtype=float, order='F')

        for i in range(0, POP_SIZE):                    
            for j in range(0, D):
                if j < D_S1:
                    PP_S1_G[i][j] = _PP_S1_G[i][j]
                else:
                    PP_S1_G[i][j] = 0.0

                if j < D_S2:        
                    PP_S2_G[i][j] = _PP_S2_G[i][j]
                else:
                    PP_S2_G[i][j] = 0.0


        X_train, X_test, Y_train, Y_test = train_test_split(PP_S1_G, PP_S2_G, test_size=0.1)

        self.autoencoders[index].fit(X_train, Y_train,
                        epochs=10,
                        batch_size=52,
                        #shuffle=True,
                        verbose=False)
                             

        self.loss_values[index] = self.autoencoders[index].evaluate(X_test, Y_test, verbose=False)        

        # try:
        #     m1 = np.matmul(PP_S2_G, np.transpose(PP_S1_G))                        
        #     m2 = np.matmul(PP_S1_G, np.transpose(PP_S1_G))
        #     m2 = np.linalg.pinv(m2)
        #     M = np.matmul(m1, m2)
        #     #print("Deu bom")
        #     return M
        # except np.linalg.linalg.LinAlgError:
        #     print(test_loss"Unexpected error:", sys.exc_info()[0])
        #     return []
        #print("Losses:" + str(self.loss_values))
            

    def map_optimized_solutions(self, pop, G, index=0, M = None):

        #cria matriz de mapeamento
        #M = self.train(G, pop, index)

        #if M == []:
        #    return []

        if self.past_problems[index].decision_space_dimension < self.current_problem.decision_space_dimension:

            optimized_solutions = np.ndarray(shape=(len(self.past_problems[index].optimized_solutions), \
            self.current_problem.decision_space_dimension), dtype=float, order='F')

            for i in range(0, self.past_problems[index].population_size):
                for j in range(0, self.current_problem.decision_space_dimension):

                    if j < self.past_problems[index].decision_space_dimension:
                        optimized_solutions[i][j] = self.past_problems[index].optimized_solutions[i][j]
                    else:
                        optimized_solutions[i][j] = 0.0

        else:
            optimized_solutions = self.past_problems[index].optimized_solutions

       
        predicted = self.autoencoders[index].predict(optimized_solutions)

        #predicted = np.matmul(M, optimized_solutions)        
        
        # for i,row in enumerate(predicted):
        #     mudancas=0
        #     for j,val in enumerate(row):
        #         # if val >=-1 and val < 0:
        #         #     predicted[i][j] *= -1
        #         #     mudancas+=1
        #         # elif val < -1 or val > 1:
        #         #     predicted[i][j] = random.uniform(0, 1)
        #         #     mudancas+=1
        #         if val < 0 or val > 1:
        #              predicted[i][j] = random.uniform(0, 1)
        #              mudancas+=1

            #print(mudancas, " mudanças")
       

        if self.past_problems[index].decision_space_dimension != self.current_problem.decision_space_dimension:
        
            learned_solutions = np.ndarray(shape=(len(predicted), self.current_problem.decision_space_dimension), \
            dtype=float, order='F')

            for i in range(0, len(predicted)):
                for j in range(0, self.current_problem.decision_space_dimension):
                    learned_solutions[i][j] = predicted[i][j]
        else:
            learned_solutions = predicted
        

        return learned_solutions


    def reset(self):
        self._first_training = True