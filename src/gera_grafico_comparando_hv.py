from os import listdir
from os.path import isfile, join
import pandas as pd
import numpy as np
import matplotlib.pyplot as plt

plt.style.use(['seaborn-paper'])


def gera_grafico(titulo, df1, df2, df3, df4, legenda1, legenda2, legenda3, legenda4, arquivo_saida, ylim=[0, 0.7]):

        plt.figure(figsize=(5,4))
        #plt.title(titulo)
        plt.xlabel("NFE")
        plt.ylabel("Hipervolume")

        plt.xlim([0, 14000])
        #plt.ylim(ylim)

        plot_hv(plt, df1, legenda1)
        #plot_hv(plt, df2, legenda2)
        #plot_hv(plt, df3, legenda3)
        plot_hv(plt, df4, legenda4)

        plt.legend(loc='center right')
        plt.savefig("../output/graficos/" + arquivo_saida)


def plot_hv(plt, df, label, marker='o', linestyle='--', cor=""):
        
    plt.plot(df[df.columns[0]], df[df.columns[1]], marker=marker, linestyle=linestyle, label=label)


def zdt(problema):

    hv1 = pd.DataFrame(np.loadtxt("../output/zdt"+problema+"/hv/media/hv.dat"))
    hv2 = pd.DataFrame(np.loadtxt("../output/zdt"+problema+"_transfer_zdt/hv/media/hv.dat"))
    hv3 = pd.DataFrame(np.loadtxt("../output/zdt"+problema+"_transfer_todos/hv/media/hv.dat"))
    hv4 = pd.DataFrame(np.loadtxt("../output/zdt"+problema+"_transfer_todos_min/hv/media/hv.dat"))

    gera_grafico("Hipervolume médio ao longo da otimização", hv1, hv2, hv3, hv4, \
    "NSGA-II", "NSGA-II + transfer zdt", "NSGA-II + transfer todos", "NSGA-II + transfer todos (min)",\
    "zdt"+problema+"_transfer.pdf", [0.0, 1.0])


def two_member_truss():

    hv1 = pd.DataFrame(np.loadtxt("../output/two_member_truss/hv/media/hv.dat"))
    hv2 = pd.DataFrame(np.loadtxt("../output/two_member_truss_transfer_todos_min/hv/media/hv.dat"))

    #hv3 = pd.DataFrame(np.loadtxt("../output/zdt"+problema+"_transfer_todos/hv/media/hv.dat"))
    hv4 = pd.DataFrame(np.loadtxt("../output/two_member_truss_transfer_todos_min_sig/hv/media/hv.dat"))

    gera_grafico("Hipervolume médio ao longo da otimização", hv1, hv1, hv1, hv2, \
    "NSGA-II", "NSGA-II + transfer zdt", "NSGA-II + transfer todos", "NSGA-II + transfer todos (min)",\
    "two_member_truss_transfer.pdf", [0.0, 1.0])

def welded_beam():

    hv1 = pd.DataFrame(np.loadtxt("../output/welded_beam/hv/media/hv.dat"))
    #hv2 = pd.DataFrame(np.loadtxt("../output/welded_beam/hv/media/hv.dat"))

    #hv3 = pd.DataFrame(np.loadtxt("../output/zdt"+problema+"_transfer_todos/hv/media/hv.dat"))
    hv4 = pd.DataFrame(np.loadtxt("../output/welded_beam_transfer_todos_min/hv/media/hv.dat"))

    gera_grafico("Hipervolume médio ao longo da otimização", hv1, hv1, hv1, hv4, \
    "NSGA-II", "NSGA-II + transfer zdt", "NSGA-II + transfer todos", "NSGA-II + transfer todos (min)",\
    "welded_beam.pdf", [0.0, 1.0])


def metal_cutting():

    hv1 = pd.DataFrame(np.loadtxt("../output/metal_cutting/hv/media/hv.dat"))
    #hv2 = pd.DataFrame(np.loadtxt("../output/welded_beam/hv/media/hv.dat"))

    #hv3 = pd.DataFrame(np.loadtxt("../output/zdt"+problema+"_transfer_todos/hv/media/hv.dat"))
    hv4 = pd.DataFrame(np.loadtxt("../output/metal_cutting_transfer_todos_min/hv/media/hv.dat"))

    gera_grafico("Hipervolume médio ao longo da otimização", hv1, hv1, hv1, hv4, \
    "NSGA-II", "NSGA-II + transfer zdt", "NSGA-II + transfer todos", "NSGA-II + transfer todos (min)",\
    "metal_cutting.pdf", [0.0, 1.0])



# zdt('1')
# zdt('2')
# zdt('3')
# zdt('4')
# zdt('6')

two_member_truss()
#welded_beam()
#metal_cutting()